# COVID-control-QC-analysis
### Warning: This repo is still under testing and development and is largely experimental.
### Description
Scripts for analysis of the negative control sequences from COVID-19 genomics developed by the BCC. 
These scripts analyze the noise levels in amplicons from COVID-19 sequence data, based on the 
ARTIC V3 amplicon-based sequencing protocol. The data obtained from these scripts can help 
quantify noise and assess the impact of noise on variant calls. 

##### Created by: Karthikeyan Sivaraman and Esha Joshi
##### Organization: Public Health Ontario
###### Date: 03 December 2020

### Usage and General Notes
The script uses information from processed COVID-19 data [SNPs and Overall Readcounts] and 
calculates the conditional probability of noise, given an amplicon -- the assumption is that any variant in that amplicon will exhibit the same level of noise. This doesn't take into account the read depth of the SNPs in question and any subsequent effects.

### Required Inputs

The script uses the following data:
#### 1. sampleData.tsv - a table of N samples with X amplicons and plate information. If your data is 
   processed using OICR's [ncov-tools pipeline](https://github.com/jts/ncov-tools), you can use 
   the `amplicon_coverage_table.tsv` file outputs with an added column for the Plate/run ID. The 
   format for this table is as follows:
    1. Column 1: Plate 
    2. Column 2: Sample ID or names
    3. Column 3-100: Amplicon read count 
    
The formatting and header should look like the following: 

   ``` 
   plate    sample_ID   amp1    amp2    amp3   ...  amp98
   ```
   
#### 2. negData.tsv - a similar table as `sampleData.tsv` above, but for the negative control 
sample 
data 
    
#### 3. amplicon_var_frequency.tbl - a four column tab delimited table with the following columns:
   1. Amplicon ID
2. Total sample number
3. Var_frequency in Amplicon: Number of times in all samples that an amplicon contains 
       atleast one variant.
   
4. Var_frequency / Total Samples: The frequency from above over the total samples which is a 
       constant.
       
#### 4. Noise_var_frequency.tbl - Similar to #3 but for noise data. 
#### NOTE: There is a need to create these two tables using bespoke scripts - either Python or PERL. A good way to generate these scripts: do a bedtools intersect between the variant list (for a particular plate) and the amplicon BED file. 
#### Then, count the number of times that an amplicon carries at least one variant. DO NOT COUNT NUMBER OF VARIANTS in EACH AMPLICON. The script normalizes the frequency of mutated amplicon only. 
    
------


#### The scripts performs the following steps:

1. For each amplicon, Calculate p(VarFreq | Noise) as follows: total variant in amplicon / no. of negative control samples
        a. Where (1) = 0, set a default minimum of p = 0.001 [arbitrary]
   
2. Convert raw noise in each negative control amplicon to a fraction of total data per sample, as follows:
        `p(noise) = noise / (data + noise)`
   
3. Calculate p(Variant) as follows:
        `p(Variant) = sum ( p(VarFreq | Noise) * p(noise) ) [for all amplicons]`
   
   
4. Calculate conditional probability p(Noise | Variant in Amplicon) [or more simply, p(Noise | Variant)] as follows:
        `p(N | V) for amplicon = p(V | N) . p(N) [for amplicon] / p(Variant) [from (3)]`
   
### Outputs
The script will save the results for conditional probabilities for each sample provided in a an 
excel sheel for sharing as well as barplots to visualize the noise levels across each amplicon. 
   
________
________
#### If you have any questions about the script or this analysis, please contact the BCC at 
support@coreontario.ca.